package mainPackage;
//Brainchild of: Anthony Vejar, Sarah Walling, and Derek Hickman
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Player {

	private String playerName;
	private String playerImage;
	int itemsCollected;
	int playerScore;
	

	public Player(String playerName, String playerImage, int itemsCollected, int playerScore){
		
		this.playerName = playerName;
		this.playerImage = playerImage;
		this.itemsCollected = itemsCollected;
		this.playerScore = playerScore;
		
	}
	
	@Override
	public String toString() {
		return "Player playerName=" + playerName + ", playerImage=" + playerImage + ", itemsCollected="
				+ itemsCollected + ", playerScore=" + playerScore;
	}

	public int getScore()
	{
		
		return playerScore;
	}
	
}
