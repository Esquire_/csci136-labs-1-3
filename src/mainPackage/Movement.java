package mainPackage;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
//Brainchild of: Anthony Vejar, Sarah Walling, and Derek Hickman
public class Movement extends JPanel implements KeyListener {
	int p1x = 500;
	int p2x = 400;
	int p1y = 500;
	int p2y = 500;
	
	
	@Override
	public void keyPressed(KeyEvent arg0) {
		int keyCode = arg0.getKeyCode();
		
		if (keyCode == KeyEvent.VK_LEFT) 
		{

			p1x -= 100;
		}
		else if (keyCode == KeyEvent.VK_RIGHT) 
		{

			p1x += 100;
		}
		else if (keyCode == KeyEvent.VK_UP) 
		{

			p1y -= 100;
		}
		else if (keyCode == KeyEvent.VK_DOWN) 
		{

			p1y += 100;
		}	if (keyCode == KeyEvent.VK_A) 
		{

			p2x -= 100;
		}
		else if (keyCode == KeyEvent.VK_D) 
		{

			p2x += 100;
		}
		else if (keyCode == KeyEvent.VK_W) 
		{

			p2y -= 100;
		}
		else if (keyCode == KeyEvent.VK_S) 
		{

			p2y += 100;
			
		}
		
		repaint();
		
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}




/*private boolean areRectsColliding(int r1TopLeftX, int
r1BottomRightX,int r1TopLeftY, int r1BottomRightY, int
r2TopLeftX,int r2BottomRightX, int r2TopLeftY, int
r2BottomRightY)
	{
		if (r1TopLeftX < r2BottomRightX && r1BottomRightX >
		r2TopLeftX&& r1TopLeftY < r2BottomRightY && r1BottomRightY >
		r2TopLeftY)
		{
			return true;
		}
		else
		{
		
			return false;
		}
	}
	*/
}

