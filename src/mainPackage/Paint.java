package mainPackage;
//Brainchild of: Anthony Vejar, Sarah Walling, and Derek Hickman
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;


public class Paint extends JPanel{
	
	int p1x = 500;
	int p2x = 400;
	int p1y = 500;
	int p2y = 500;
	
	ImageIcon myIcon = new ImageIcon("./src/mainPackage/TreasureChest.png");
	ImageIcon myIcon2 = new ImageIcon("./src/mainPackage/ghost.png");
	
	public void paintComponent(Graphics page)
	{	
		
		paintComponent(page);
		page.drawImage(myIcon.getImage(), p1x, p1y, null);
		page.drawImage(myIcon2.getImage(), p2x, p2y, null);
	}
	
	 public void repaintMethod(){
		 repaint();
		 }
	
}
