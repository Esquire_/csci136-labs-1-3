package mainPackage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class _Player {

	private String playerName;
	private String playerImage;
	int itemsCollected;
	int playerScore;

	public _Player(String playerName, String playerImage, int itemsCollected, int playerScore){
		
		this.playerName = playerName;
		this.playerImage = playerImage;
		this.itemsCollected = itemsCollected;
		this.playerScore = playerScore;
	}
	
	public void playerInfo() throws FileNotFoundException {
		
	ArrayList<Player> Players = new ArrayList<Player>();	
		
	File infoOne = new File("./src/mainPackage/playerOne.txt");
	Scanner scannerOne = new Scanner(infoOne).useDelimiter(",");
		while(scannerOne.hasNext()){
			String playerName = scannerOne.next();
			String playerImage = scannerOne.next();
			int itemsCollected = Integer.parseInt(scannerOne.next().trim());
			int playerScore = Integer.parseInt(scannerOne.next().trim());
			Player playerOne = new Player(playerName, playerImage, itemsCollected, playerScore);
			Players.add(playerOne);
		}


	File infoTwo = new File("./src/mainPackage/playerTwo.txt");
	Scanner scannerTwo = new Scanner(infoTwo).useDelimiter(",");
		while(scannerTwo.hasNext()){
			String playerName = scannerTwo.next();
			String playerImage = scannerTwo.next();
			int itemsCollected = Integer.parseInt(scannerTwo.next().trim());
			int playerScore = Integer.parseInt(scannerTwo.next().trim());
			Player playerTwo = new Player(playerName, playerImage, itemsCollected, playerScore);
			Players.add(playerTwo);
		}
	
	
	}
	
	

	int playerOneScore;
	int playerTwoScore;
	
	public int compareScores(){
		
		int higherScore = 0;
		if(playerOneScore>playerTwoScore){
			
			higherScore = playerOneScore;
		}
		else{
			
			
		}
		return higherScore;
	}
	
	
	
	
}
