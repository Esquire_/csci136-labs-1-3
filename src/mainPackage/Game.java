package mainPackage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
//Brainchild of: Anthony Vejar, Sarah Walling, and Derek Hickman
public class Game {

	Player playerOne;
	
	Player playerTwo;
	ArrayList wallObjects = new ArrayList();
	ArrayList itemObjecs = new ArrayList();
	ArrayList Scores = new ArrayList();
	
	public void getPlayerInfo() {
		ArrayList<Player> Players = new ArrayList<Player>();	
		
		File infoOne = new File("./src/mainPackage/playerOne");
		Scanner scannerOne = null;
		
		try {
			scannerOne = new Scanner(infoOne).useDelimiter(",");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(scannerOne.hasNext()){
				String playerName = scannerOne.next();
				String playerImage = scannerOne.next();
				int itemsCollected = Integer.parseInt(scannerOne.next().trim());
				int playerScore = Integer.parseInt(scannerOne.next().trim());
				playerOne = new Player(playerName, playerImage, itemsCollected, playerScore);
				Players.add(playerOne);
			}
		

		File infoTwo = new File("./src/mainPackage/playerTwo");
		Scanner scannerTwo = null;
		
		try {
			scannerTwo = new Scanner(infoTwo).useDelimiter(",");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			while(scannerTwo.hasNext()){
				String playerName = scannerTwo.next();
				String playerImage = scannerTwo.next();
				int itemsCollected = Integer.parseInt(scannerTwo.next().trim());
				int playerScore = Integer.parseInt(scannerTwo.next().trim());
				playerTwo = new Player(playerName, playerImage, itemsCollected, playerScore);
				Players.add(playerTwo);
			}
		
	}
	

	

	



	public void getScores()	{
		System.out.println("Player one score is : " + playerOne.getScore());
		System.out.println("Player two score is : " + playerTwo.getScore());
		System.out.println(playerOne + "\n" + playerTwo);
		
	}

	


}