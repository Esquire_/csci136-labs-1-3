package mainPackage;
//Brainchild of: Anthony Vejar, Sarah Walling, and Derek Hickman
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class MainPanel extends Movement{

	ImageIcon myIcon = new ImageIcon("./src/mainPackage/TreasureChest.png");
	ImageIcon myIcon2 = new ImageIcon("./src/mainPackage/ghost.png");
	
	public MainPanel()
	{
		setPreferredSize(new Dimension(1000,1000));
		addKeyListener(this);
		setFocusable(true);
		
		Game game1 = new Game();
		game1.getPlayerInfo();
		game1.getScores();
		
	}

	
	public void paintComponent(Graphics page)
	{
		super.paintComponent(page);
		page.drawImage(myIcon.getImage(), p1x, p1y, null);
		page.drawImage(myIcon2.getImage(), p2x, p2y, null);
	}
	
}
	
